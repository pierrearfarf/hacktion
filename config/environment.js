/* eslint-env node */
'use strict';

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'short',
    environment,
    rootURL: '/',
    locationType: 'auto',
        apiBaseUrl: 'http://localhost:8000',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {
     ENV.apiBaseUrl = 'http://hacktions.us-east-2.elasticbeanstalk.com';
  }
  ENV['ember-simple-auth'] = {
   authorizer: 'authorizer:token',
   routeAfterAuthentication: '/project/all',
   routeIfAlreadyAuthenticated:'/project/all'
  };
  ENV['ember-simple-auth-token'] = {
     refreshAccessTokens: true,
     timeFactor: 1000,
     refreshLeeway: 300,
     serverTokenEndpoint: ENV.apiBaseUrl + '/api/user/auth',
     serverTokenRefreshEndpoint: ENV.apiBaseUrl + '/api/user/token-refresh',
     identificationField: 'email',
     passwordField: 'password',
     tokenExpireName: 'exp',
  };
  return ENV;
};
