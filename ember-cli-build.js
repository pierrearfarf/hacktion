/* eslint-env node */
'use strict';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {

  let app = new EmberApp(defaults, {
   'ember-bootstrap-datetimepicker': {
   "importBootstrapCSS": false,
   "importBootstrapJS": false,
   "importBootstrapTheme": false,
   outputPaths: {
     app: {
       css: {
          'app': '/assets/app.css',
       }
     }
   }
},
'ember-cli-bootstrap-sass': {
 'importBootstrapJS': true
},
ace: {
themes: ['ambiance','cobalt','github'],
modes: ['json','javascript'],
workers: ['json','javascript'],
exts:['beautify','searchbox']
}
  });

  app.import('vendor/ionicons/ionicons.otf', {
      destDir: 'fonts'
  });
  app.import('vendor/ionicons/ionicons.eot', {
      destDir: 'fonts'
  });
  app.import('vendor/ionicons/ionicons.svg', {
      destDir: 'fonts'
  });
  app.import('vendor/ionicons/ionicons.ttf', {
      destDir: 'fonts'
  });
  app.import('vendor/ionicons/ionicons.woff', {
      destDir: 'fonts'
  });
  app.import('vendor/ionicons/ionicons.woff2', {
      destDir: 'fonts'
  });





  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  app.import('bower_components/bootstrap/dist/js/bootstrap.js');
    app.import('vendor/gif/b64.js');
    app.import('vendor/gif/LZWEncoder.js');
    app.import('vendor/gif/NeuQuant.js');
    app.import('vendor/gif/GIFEncoder.js');

    return app.toTree();
  };
