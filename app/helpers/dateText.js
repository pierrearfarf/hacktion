import Ember from 'ember';
import moment from 'moment';

export function dateText(date/*, hash*/) {
   return moment(date[0]).locale('fr').format("dddd D MMMM");
}

export default Ember.Helper.helper(dateText);
