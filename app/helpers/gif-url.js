import Ember from 'ember';
import ENV from '../config/environment';

export function gifUrl(url/*, hash*/) {
  return ENV.apiBaseUrl +'/images/'+url;
}

export default Ember.Helper.helper(gifUrl);
