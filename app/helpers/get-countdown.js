import Ember from 'ember';
import moment from 'moment';

export function getCountdown(date/*, hash*/) {
   return moment(date[0]).fromNow(true);

}

export default Ember.Helper.helper(getCountdown);
