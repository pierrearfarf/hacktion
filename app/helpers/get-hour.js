import Ember from 'ember';
import moment from 'moment';

export function getHour(date/*, hash*/) {
   return moment(date[0]).format('HH:mm');
}

export default Ember.Helper.helper(getHour);
