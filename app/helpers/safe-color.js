import Ember from 'ember';

export function safeColor(params/*, hash*/) {
   return Ember.String.htmlSafe("background: " + params);
}

export default Ember.Helper.helper(safeColor);
