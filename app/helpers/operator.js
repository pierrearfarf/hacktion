import Ember from 'ember';

export function operator(params) {
    var lvalue = parseFloat(params[0]);
    var rvalue = parseFloat(params[2]);
    var operator = params[1];
   var count;
   if(operator == "+"){
      count = lvalue + rvalue;
   }
   return count;

}

export default Ember.Helper.helper(operator);
