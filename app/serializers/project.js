import DS from 'ember-data';

export default DS.JSONAPISerializer.extend(DS.EmbeddedRecordsMixin,{
attrs: {
    images: { embedded: 'always' }
},
keyForAttribute: function(attr) {
return Ember.String.camelize(attr);
}
});
