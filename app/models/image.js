import DS from 'ember-data';

export default DS.Model.extend({
project: DS.belongsTo('project'),
position: DS.attr('number'),
imageData: DS.attr('raw'),
imageFile: DS.attr('string'),
fullscreen: false
});
