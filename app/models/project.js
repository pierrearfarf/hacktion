import DS from 'ember-data';

export default DS.Model.extend({
   title: DS.attr('string'),
   userId: DS.attr('number'),
   images: DS.hasMany('image'),
   functions: DS.attr('raw'),
   width: DS.attr('number'),
   export: DS.attr('string'),
   bgcolor: DS.attr('string'),
   color: DS.attr('string'),
   grid:DS.attr('boolean'),
   gridcolor: DS.attr('string'),
   draft: DS.attr('string'),

});
