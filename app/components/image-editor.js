import Ember from 'ember';

export default Ember.Component.extend({
  store: Ember.inject.service(),
  data: null,
  active: false,
  classNameBindings: ['active'],
  width: null,
  height: Ember.computed('width', function() {
    return this.get('width') / 1.6;
  }),
  rows: Ember.computed('height', 'width', 'canvas', function() {

    const canvas = this.get("canvas");
    var rows = [];
    let width = this.get('width');
    let height = width / 1.6;
    var i = 1;
    var j = 1;
    var p = 0
    for (i = 1; i <= height; i++) {
      var row = [];
      for (j = 1; j <= width; j++) {
        var id = j.toString() + ':' + i.toString();
        var pixel = canvas[p];
        row.pushObject(pixel);
        p++;
      }
      rows.pushObject(row);
    }

    return rows;
  }),
  canvas: Ember.computed('blankCanvas', 'image', function() {
    var blankCanvas = this.get('blankCanvas');
    var data = [];

    var objects = this.get("image.imageData.objects");
    if (objects) {
      objects.forEach(function(object) {
        data = data.concat(object.pixels);

      });
    }
    if(this.get("image.imageData.pixels") == undefined){
      this.set('image.imageData.pixels', []);
   }
   else{
      // data = this.get("image.imageData.pixels").concat(data);
      data = data.concat(this.get("image.imageData.pixels"));

   }

    var length = blankCanvas.length;
    var j;
    var i;
    var tempObj = [];
    data.reverse();
    for (i = 0; i < length; i++) { //Loop trough first array
      var curID = blankCanvas[i].id; //Get ID of current object
      var exists = false;
      for (j = 0; j < data.length; j++) { //Loop trough second array
        exists = false;
        if (curID == data[j].id) { //If id from array1 exists in array2
          exists = true;
          tempObj = data[j]; //Get id,object from array 2
          break;
        }
      }
      if (exists) {
        blankCanvas[i]["color"] = tempObj.color; //If exists add circle from array2 to the record in array1
      } else {
        blankCanvas[i]["color"] = "transparent"; //If it doesn't add circle with value "no"
      }
    }
    return blankCanvas;
  }),
  blankCanvas: Ember.computed('height', 'width', function() {
    var rows = [];
    let width = this.get('width');
    let height = width / 1.6;
    var i = 1;
    var j = 1;
    for (i = 1; i <= height; i++) {
      for (j = 1; j <= width; j++) {
        var id = j.toString() + ':' + i.toString();
        var pixel = {
          "id": id,
        };
        rows.pushObject(pixel);
      }
    }
    return rows;
  }),

  didInsertElement() {
    this.$('.pixel').on('mouseover', function() {
        $('.row.row-active').removeClass('row-active');
        $('.pixel.col-active').removeClass('col-active');
        var index = $(this).index();
        $(this).parent().closest('.grid').find('.row:first-child').find('.pixel').eq(index).addClass('col-active');
        $(this).closest('.row').addClass('row-active');
      }),
      this.sendAction('sendLoadedImage', 1);
  },
  actions: {
    pixelInfo:function(pixel){
      this.sendAction('setCurrentPixel', pixel.id);
   },
    pixelUp: function(pixel) {
      var pixels = [];
      var data = this.get('image.imageData.pixels');
      var objects = this.get('image.imageData.objects');
      var imageId = this.get('image.id');
      var imagePostion = this.get('image.position');
      var color = this.get('color');
      var isExist = "false";
      for (var x in data) {
        if (data[x].id === pixel.id) {
          isExist = x;
        }
      }
      if (isExist != "false") {

        var i = isExist;
        var obj = data[i];
        data.removeObject(obj);

      } else {
        var newpixel = {
          "id": pixel.id,
          "color": color
        };
        data.pushObject(newpixel);
        this.set('unSave', true);
      }

      // data.sort(this.dynamicSort("id"));

      var imageData = {
        "image": imagePostion,
        "pixels": data,
        "objects": objects
      };

      var image = this.get('store').peekRecord('image', imageId);
      image.set('imageFile', JSON.stringify(imageData, null, "\t"));
      image.set('imageData', imageData);
    }
  },

  canvasDidChange: Ember.on('didUpdateAttrs', Ember.observer('image.imageData', function() {
    console.log("CHANGE CHANGE CHANGE");
    var blankCanvas = this.get('blankCanvas');
    var object = this.get('store').peekRecord('image', this.get('image.id'));

    var data = [];
    //  var data = jsonData.pixels;

    var objects = object.get("imageData.objects");
    if (objects) {
      objects.forEach(function(object) {
      //   data = object.pixels.reverse().concat(data);
        data = data.concat(object.pixels);
      // data = object.pixels;
      });
      // objects.reverse();
    }
    data = data.concat(object.get('imageData.pixels'));
   //  data = object.get('imageData.pixels').concat(data);

    var length = blankCanvas.length;
    var j;
    var i;
    var tempObj = [];
    data.reverse();
    for (i = 0; i < length; i++) { //Loop trough first array
      var curID = blankCanvas[i].id; //Get ID of current object
      var exists = false;
      for (j = 0; j < data.length; j++) { //Loop trough second array
        exists = false;
        if (curID == data[j].id) { //If id from array1 exists in array2
          exists = true;
          tempObj = data[j]; //Get id,object from array 2
          break;
        }
      }
      if (exists) {
        var obj = blankCanvas[i];
        Ember.set(obj, 'color', tempObj.color);
      } else {
        var obj = blankCanvas[i];
        Ember.set(obj, 'color', "transparent");
      }
    }
  })),

  dynamicSort: function(property) {
    var sortOrder = 1;
    if (property[0] === "-") {
      sortOrder = -1;
      property = property.substr(1);
    }
    return function(a, b) {
      var result = (a[property] < b[property]) ? -1 : (a[property] > b[property]) ? 1 : 0;
      return result * sortOrder;
    }
  }

});
