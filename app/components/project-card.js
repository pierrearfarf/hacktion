import Ember from 'ember';

export default Ember.Component.extend({
   store: Ember.inject.service(),
   actions:{
         deleteProject : function(project){
            this.get('store').findRecord('project', project, { backgroundReload: true }).then(function(project) {
              project.destroyRecord();
            });
      }
   }
});
