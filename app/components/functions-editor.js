import Ember from 'ember';

export default Ember.Component.extend({

  updtImages: [],
  errors: [],
  compile: "compile test",
   errorsSize : null,
  actions: {
    runScript: function(functions) {
      var _this = this;
      this.set('updtImages', []);
      // _this.$('.btn-event-function').attr('data-loading', 'true');
      _this.$('.btn-event-function').addClass('loading');
      // this._parse(_this, functions).then(function(response) {
      //   _this.set('compileErrors', [])
      //   _this.sendAction('scriptUpdate', response);
      //   _this.set('errorsSize', _this.get('errors.length'));
      //
      //
      // }, function(err) {
      //    //  console.log('test Error', err); // Error: "It broke"
      // });
    }
  },




  _parse: function(_this, functions) {
    return new Promise(function(resolve, reject) {
      if (functions) {
        _this.set("errors", []);
        var regExp = /\$([^;]+)\;/g;
        var instructions = functions.match(regExp);
        var updt = {};
        var images = _this.get("updtImages");
        instructions.reverse();
        console.log(instructions);
        instructions.forEach(function(cmds) {
          //  var updateObject = false;
          //  var updateImage = false;

          var commands = cmds.split(".");
          var parseCommands = [];
          parseCommands['draw'] = [];
          parseCommands['actions'] = [];
          //          console.log('commands', commands);
          // commands.reverse();
          //         console.log('commands', commands);
          //init commands
          commands.forEach(function(cmd) {
            var reg = /(.*?)[\(](.*?)[\)]/ig;
            var matches = [];
            var command = {};

            while (matches = reg.exec(cmd)) {
              command.type = matches[1];
              command.value = matches[2];
              if (command.type == 'rect' || command.type == 'circle' || command.type == 'line' || command.type == 'ellipse') {
                parseCommands['draw'].unshift(command);
              } else if (command.type == 'moveTo') {
                parseCommands['actions'].push(command);
              } else {
                if (command.type != "image") {
                  var error = {};
                  error.object = "Erreur de syntax";
                  error.detail = cmds
                  _this.errors.addObject(error);
                }
              }
            }
          });


          // OBJECT NAME & IMAGE ID
          var objName = commands[0].substring(1);
          var regId = /image\((.*)\)/i;

          if (commands[1].match(regId)) {
            var imagesId = commands[1].match(regId)[1].split(",");

          } else {
            var imagesId = [];
            var error = {};
            error.objectName = objName;
            error.imageId = '???';
            error.detail = "Aucune image n'est indiqué pour l'objet : " + objName;
            _this.errors.addObject(error);
          }




          imagesId.forEach(function(imageId) {
            imageId = Number(imageId);
            console.log("imageId", imageId);

            // INIT
            var image = _this.makeImage(imageId);
            var object = {};
            object.name = objName;
            var drawing = function(commands) {
              console.log('drawing');
              return new Promise(function(resolve, reject) {
                console.log("objName", objName);
                if (commands.length == 0) {
                  object = _this.getObject(imageId, objName);
                  resolve();
                }
                commands.forEach(function(command) {

                  if (command.type == "rect") {
                    _this.drawRect(_this, command.value).then(function(response) {
                      object = _this.updateObject(imageId, objName, response, 'drawRect');
                      resolve();
                    }, function(err) {
                      var objects = err;
                      objects.forEach(function(object) {
                        var error = {}
                        error.objectName = objName;
                        error.imageId = imageId;
                        error.object = object.object;
                        _this.errors.addObject(error);
                      });
                      reject(objects);
                    });
                  }
                  if (command.type == "circle") {
                    _this.drawCircle(_this, command.value).then(function(response) {
                      object = _this.updateObject(imageId, objName, response, 'drawCircle');
                      resolve();
                    }, function(err) {

                      var objects = err;
                      console.log("objects errors", objects);
                      objects.forEach(function(object) {
                        var error = {}
                        error.objectName = objName;
                        error.imageId = imageId;
                        error.object = object.object;
                        _this.errors.addObject(error);
                      });
                      reject(_this.errors);
                    });
                  }
                  if (command.type == "ellipse") {
                    _this.drawEllipse(_this, command.value).then(function(response) {
                      object = _this.updateObject(imageId, objName, response, 'drawEllipse');
                      resolve();
                    }, function(err) {
                      var objects = err;
                      console.log("objects errors", objects);
                      objects.forEach(function(object) {
                        var error = {}
                        error.objectName = objName;
                        error.imageId = imageId;
                        error.object = object.object;
                        _this.errors.addObject(error);
                      });
                      reject(objects);
                    });
                  }
                  if (command.type == "line") {
                    _this.drawLine(_this, command.value).then(function(response) {
                      object = _this.updateObject(imageId, objName, response, "drawLine");
                      resolve();
                    }, function(err) {
                      var objects = err;
                      console.log("objects errors", objects);
                      objects.forEach(function(object) {
                        var error = {}
                        error.objectName = objName;
                        error.imageId = imageId;
                        error.object = object.object;
                        _this.errors.addObject(error);
                      });
                      reject(objects);
                    });
                  }
                });
              });
            }
            drawing(parseCommands['draw']).then(function(response) {
              var actionsLength = parseCommands['actions'].length;
              var i = 0;
              parseCommands['actions'].forEach(function(command) {
                i++;
                if (command.type == "moveTo") {
                  var object = _this.getObject(imageId, objName);
                  console.log("imageId --", imageId);
                  console.log(" -- objName", objName);
                  console.log('---->', object);
                  _this.moveTo(command.value, imageId, object).then(function(response) {}, function(err) {
                    console.log(err);
                  });
                }
              });

              if (i == actionsLength) {
                updt = _this.get('updtImages');
                resolve(updt);
              }

            }, function(err) {
              reject(err);
            });

          });
        });
      } else {
        var error = {
          "description": 'test'
        };
        reject(error);
      }
    });
  },

  drawRect: function(_this, val) {
    return new Promise(function(resolve, reject) {

      //Check value length
      var value = val.split(',');
      var errors = [];
      // if not right set error
      if (value.length != 5) {
        var error = {};
        error.object = "Arguments attendu pour rect()";
        error.detail = 'rect(x1, y1, longeur, hauteur, couleur)';
        errors.push(error);
      }
      // else keep going
      else {
        // check data integrity
        var rectX, rectY, rectW, rectH, color;
        if (_this.verifNumber(value[0])) {
          rectX = Number(value[0]);
        } else {
          var error = {};
          error.object = "Le premier argument de rect() n'est pas un chiffre";
          errors.push(error);
        }
        //
        if (_this.verifNumber(value[1])) {
          rectY = Number(value[1]);
        } else {
          var error = {};
          error.object = "Le second argument de rect() n'est pas un chiffre";
          errors.push(error);
        }
        //
        if (_this.verifNumber(value[2])) {
          rectW = Number(value[2]);
        } else {
          var error = {};
          error.object = "Le troisième argument de rect() n'est pas un chiffre";
          errors.push(error);
        }
        //
        if (_this.verifNumber(value[3])) {
          rectH = Number(value[3]);
        } else {
          var error = {};
          error.object = "Le quatrième argument de rect() n'est pas un chiffre";
          errors.push(error);
        }
        color = value[4].replace(/\s+/g, '').slice(1, -1);
      }
      // data integrity ok : keep going
      if (rectX && rectY && rectW && rectH && color) {
        var pixels = [];
        for (var i = 0; i < rectW; i++) {
          for (var j = 0; j < rectH; j++) {
            var pixel = {};
            var x = rectX + i;
            var y = rectY + j;
            pixel.id = x + ':' + y;
            pixel.color = color;
            pixels.push(pixel);
          }
        }
        var object = {}
        object.pixels = pixels;
      }
      //if no errors relove
      if (errors.length == 0) {
        resolve(object);
      } else {
        reject(errors);
      }
    });
  },
  drawCircle: function(_this, val) {
    return new Promise(function(resolve, reject) {
      var value = val.split(',');
      var errors = [];
      //Check value length
      // if not right set error
      if (value.length != 4) {
        var error = {};
        error.object = "Arguments attendu pour circle()";
        error.detail = 'circle(centreX, centreY, rayon, couleur)';
        errors.push(error);
      }
      // else keep going
      else {
        var cx, cy, r, color;
        if (_this.verifNumber(value[0])) {
          cx = Number(value[0]);
        } else {
          var error = {};
          error.object = "Le premier argument de circle() n'est pas un chiffre";
          errors.push(error);
        }
        if (_this.verifNumber(value[1])) {
          cy = Number(value[1]);
        } else {
          var error = {};
          error.object = "Le second argument de circle() n'est pas un chiffre";
          errors.push(error);
        }
        if (_this.verifNumber(value[2])) {
          r = Number(value[2]);
        } else {
          var error = {};
          error.object = "Le troisième argument de circle() n'est pas un chiffre";
          errors.push(error);
        }
        var color = value[3].replace(/\s+/g, '').slice(1, -1);
      }

      if (cx && cy && r && color) {
        var pixels = [];

        for (r; r > 0; r--) {
          for (let i = -r; i <= r; i += 1) {
            for (let j = -r; j <= r; j += 1) {
              if (Math.round(Math.sqrt(i * i + j * j)) === r) {
                var pixel = {};
                pixel.id = (i + cx) + ':' + (j + cy);
                pixel.color = color;
                pixels.push(pixel);
              }
            }
          }
        }
        var object = {}
        //add center;
        var pixel = {};
        pixel.id = (cx) + ':' + (cy);
        pixel.color = color;
        pixels.push(pixel);
        object.pixels = pixels;
      }
      console.log('okkkkkkkk', errors);
      if (errors.length == 0) {
        console.log('okkkkkkkk');
        resolve(object);
      } else {
        reject(errors);
      }
    });
  },
  drawLine(_this, val) {
    return new Promise(function(resolve, reject) {
      var value = val.split(',');
      var errors = [];
      // if not right set error
      if (value.length != 5) {
        var error = {};
        error.object = "Arguments attendu pour line()";
        error.detail = 'line(x1, y1, x2, y2, couleur)';
        errors.push(error);
      }
      // else keep going
      else {
        // check data integrity
        var x0, y0, x1, y1, color;
        var pixels = [];

        if (_this.verifNumber(value[0])) {
          x0 = Number(value[0]);
        } else {
          var error = {};
          error.object = "Le premier argument de line() n'est pas un chiffre";
          errors.push(error);
        }
        if (_this.verifNumber(value[1])) {
          y0 = Number(value[1]);
        } else {
          var error = {};
          error.object = "Le deuxième argument de line() n'est pas un chiffre";
          errors.push(error);
        }
        if (_this.verifNumber(value[2])) {
          x1 = Number(value[2]);
        } else {
          var error = {};
          error.object = "Le troisième argument de line() n'est pas un chiffre";
          errors.push(error);
        }
        if (_this.verifNumber(value[3])) {
          y1 = Number(value[3]);
        } else {
          var error = {};
          error.object = "Le quatrième argument de line() n'est pas un chiffre";
          errors.push(error);
        }
        var color = value[4].replace(/\s+/g, '').slice(1, -1);
      }
      // data integrity ok : keep going
      if (x0 && y0 && x1 && y1 && color) {
        var dx = Math.abs(x1 - x0);
        var dy = Math.abs(y1 - y0);
        var sx = (x0 < x1) ? 1 : -1;
        var sy = (y0 < y1) ? 1 : -1;
        var err = dx - dy;

        while (true) {
          var pixel = {};
          pixel.id = x0 + ':' + y0;
          pixel.color = color;

          if ((x0 == x1) && (y0 == y1)) break;
          var e2 = 2 * err;
          if (e2 > -dy) {
            err -= dy;
            x0 += sx;
          }
          if (e2 < dx) {
            err += dx;
            y0 += sy;
          }
          pixels.push(pixel);
        }
        var pixel = {};
        pixel.id = x1 + ':' + y1;
        pixel.color = color;
        pixels.push(pixel);

        var object = {}
        object.pixels = pixels;
      }
      if (errors.length == 0) {
        resolve(object);
      } else {
        reject(errors);
      }
    });
  },

  drawEllipse(_this, val) {
    return new Promise(function(resolve, reject) {
      console.log('drawEllipse');
      var value = val.split(',');
      var errors = [];
      if (value.length != 5) {
        var error = {};
        error.object = "Arguments attendu pour ellipse()";
        error.detail = 'ellipse(x1, y1, x2, y2, couleur)';
        errors.push(error);
      } else {
        // check data integrity
        var x0, y0, x1, y1, color;
        if (_this.verifNumber(value[0])) {
          x0 = Number(value[0]);
        } else {
          var error = {};
          error.object = "Le premier argument de rect() n'est pas un chiffre";
          errors.push(error);
        }
        if (_this.verifNumber(value[1])) {
          y0 = Number(value[1]);
        } else {
          var error = {};
          error.object = "Le premier argument de rect() n'est pas un chiffre";
          errors.push(error);
        }
        if (_this.verifNumber(value[2])) {
          x1 = Number(value[2]);
        } else {
          var error = {};
          error.object = "Le premier argument de rect() n'est pas un chiffre";
          errors.push(error);
        }
        if (_this.verifNumber(value[3])) {
          y1 = Number(value[3]);
        } else {
          var error = {};
          error.object = "Le premier argument de rect() n'est pas un chiffre";
          errors.push(error);
        }
        var color = value[4].replace(/\s+/g, '').slice(1, -1);

      }

      if (x0 && y0 && x1 && y1 && color) {
        var pixels = [];
        const bottom = y1;


        var a = Math.abs(x1 - x0),
          b = Math.abs(y1 - y0),
          b1 = b & 1; /* diameter */
        var dx = 4 * (1.0 - a) * b * b,
          dy = 4 * (b1 + 1) * a * a; /* error increment */
        var err = dx + dy + b1 * a * a,
          e2; /* error of 1.step */
        var h = Math.round(b / 2);
        var w = Math.round(a / 2);

        if (x0 > x1) {
          x0 = x1;
          x1 += a;
        } /* if called with swapped points */
        if (y0 > y1) {
          y0 = y1;
        } /* .. exchange them */
        y0 += (b + 1) >> 1;
        y1 = y0 - b1; /* starting pixel */
        a = 8 * a * a;
        b1 = 8 * b * b;

        do {
          /*   I. Quadrant */
          var pixel = {};
          pixel.id = x1 + ':' + y0;
          pixel.color = color;
          pixels.push(pixel);

          for (var h1 = 0; h1 <= h - (bottom - y0); h1++) {
            var pixel = {};
            pixel.id = x1 + ':' + (y0 - h1);
            pixel.color = color;
            pixels.push(pixel);
          }

          /*  II. Quadrant */
          var pixel = {};
          pixel.id = x0 + ':' + y0;
          pixel.color = color;
          pixels.push(pixel);
          console.log("x0", x0);

          for (var h1 = 0; h1 <= h - (bottom - y0); h1++) {
            console.log("h1", h1);
            var pixel = {};
            pixel.id = x0 + ':' + (y0 - h1);
            pixel.color = color;
            pixels.push(pixel);
          }


          /* III. Quadrant */
          var pixel = {};
          pixel.id = x0 + ':' + y1;
          pixel.color = color;
          pixels.push(pixel);

          for (var h1 = 0; h1 < h - (bottom - y0); h1++) {
            console.log("h1", h1);
            var pixel = {};
            pixel.id = x0 + ':' + (y1 + h1);
            pixel.color = color;
            pixels.push(pixel);
          }

          /*  IV. Quadrant */
          var pixel = {};
          pixel.id = x1 + ':' + y1;
          pixel.color = color;
          pixels.push(pixel);
          for (var h1 = 0; h1 < h - (bottom - y0); h1++) {
            console.log("h1", h1);
            var pixel = {};
            pixel.id = x1 + ':' + (y1 + h1);
            pixel.color = color;
            pixels.push(pixel);
          }

          e2 = 2 * err;
          if (e2 <= dy) {
            y0++;
            y1--;
            err += dy += a;
          } /* y step */
          if (e2 >= dx || 2 * err > dy) {
            x0++;
            x1--;
            err += dx += b1;
          } /* x */
        } while (x0 <= x1);

        while (y0 - y1 <= b) {
          /* too early stop of flat ellipses a=1 */
          /* -> finish tip of ellipse */
          pixel = {};
          pixel.id = (x0 - 1) + ':' + y0;
          pixel.color = color;
          pixels.push(pixel);

          pixel = {};
          pixel.id = (x1 + 1) + ':' + (y0++);
          pixel.color = color;
          pixels.push(pixel);

          pixel = {};
          pixel.id = (x0 - 1) + ':' + y1;
          pixel.color = color;
          pixels.push(pixel);

          pixel = {};
          pixel.id = (x1 + 1) + ':' + (y1--);
          pixel.color = color;
          pixels.push(pixel);
        }
        var object = {}
        object.pixels = pixels;
      }

      if (errors.length == 0) {
        resolve(object);
      } else {
        reject(errors);
      }
    });
  },


  moveTo: function(val, imageId, object) {
    console.log("moveTo");
    var _this = this;
    return new Promise(function(resolve, reject) {
      var value = val.split(',');
      if (value.length == 3) {
        var x = Number(value[0]);
        var y = Number(value[1]);
        var nbrImages = Number(value[2]);

        var stepX = x / nbrImages;
        var stepY = y / nbrImages;
        //   console.log('nbrImages',nbrImages);
      }
      if (object.pixels.length != 0) {
        for (let i = 1; i <= nbrImages; i++) {
          var image = _this.makeImage(Number(imageId) + i);
          //  console.log('makeImage boucle nbrImages', image);
          //  image.image = Number(imageId) + i;
          var newObject = {};
          console.log('>>>>>>>>>>>>>', object.name);
          console.log('>>>>>>>>>>>>>   IMAGE');
          console.log(image);
          console.log('---------------------');
          newObject.name = object.name;
          newObject.pixels = [];
          // update position
          object.pixels.forEach(function(pixel) {
            var coord = pixel.id.split(":");
            var x0 = Number(coord[0]);
            var y0 = Number(coord[1]);
            var newId = Math.round(x0 + (stepX * i)) + ':' + Math.round(y0 + (stepY * i));
            // console.log('pixelId', pixel.id, "newId", newId);
            var newPixel = {};
            newPixel.id = newId;
            newPixel.color = pixel.color;
            newObject.pixels.push(newPixel);
          });
          console.log("MoveTo object before update", object);
          _this.updateObject(image.image, object.name, newObject, 'MoveTo');
          //  image.object = newObject;
          //  images.addObject(image);
        }
        //   console.log("updtImages",_this.get('updtImages'))
        resolve(_this.get('updtImages'));
      } else {
        this.set('errors', "mouette");
        reject(this.get('errors'));
      }
    });

  },
  verifNumber: function(val) {
    if (Number(val)) {
      console.log("verifNumber true");
      return true;
    } else {
      console.log("verifNumber false");
      return false;
    }
  },
  makeImage: function(imageId) {
    var images = this.get('updtImages');
    // console.log('>>',images);
    var image = images.findBy('image', Number(imageId));
    // console.log('>>+',image);
    if (image == undefined) {
      var image = {};
      image.image = Number(imageId);
      image.objects = [];
      images.addObject(image);
    }
    return image;
  },
  updateObject: function(imageId, objName, updatedobject, type) {
    var images = this.get('updtImages');
    // console.log('images test', images);
    var image = images.findBy('image', Number(imageId));
    // console.log(objName);
    var object = image.objects.findBy('name', objName);
    if (object == undefined) {
      console.log("-------------------    create" + objName);
      var object = {};
      object.name = objName;
      object.pixels = updatedobject.pixels;
      if (type != "moveTo") {
        image.objects.insertAt(0, object);
      } else {
        image.objects.pushObject(object);
      }
    } else {
      console.log("-------------------    update" + objName);
      var oldData = object.pixels;
      var newData = updatedobject.pixels;
      object.pixels = newData.concat(oldData);
      //  object.pixels = oldData.concat(newData);
    }
    return object;
  },
  getObject: function(imageId, objName) {
    var images = this.get('updtImages');
    //   console.log('images test', images);
    var image = images.findBy('image', Number(imageId));
    //   console.log(':poutpout:',image.objects);
    var object = image.objects.findBy('name', objName);
    return object;
  }

});
