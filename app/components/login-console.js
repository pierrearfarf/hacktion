import Ember from 'ember';

export default Ember.Component.extend({
  session: Ember.inject.service(),
  sessionAccount: Ember.inject.service(),
  step1: true,
  step2: false,
  identification: null,
  password: null,
  inscription: false,
  inscription1: false,
  inscription2: false,
  inscription3: false,
  inscription4: false,
  welcome:true,
  didRender() {
    if (this.step1) {
      this.$("#identification").focus();
    }
    if (this.step1 && this.step2) {
      this.$('#password').focus();
    }
    if (this.inscription1) {
      this.$('#newIdentification').focus();
    }
    if (this.inscription1 && this.inscription2) {
      this.$('#newMail').focus();
    }
    if (this.inscription1 && this.inscription3) {
      this.$('#newPassword').focus();
    }
    if (this.inscription1 && this.inscription4) {
      this.$('#password').focus();
    }
    if (this.errorLogin  && this.connexion) {
      if(this.inscription && this.inscription5){
         this.$('<p class="error">' + this.errorMessage + '</p>').appendTo('.ticket2');
         this.set('step1', true);
         this.set('erroLogin', false);
         this.set('connexion', false);
      }
      else{
         this.set('identification', null);
         this.$('<p class="error">' + this.errorMessage + '</p>').appendTo('.ticket');
         this.set('step1', true);
         this.set('step2', false);
         this.set('erroLogin', false);
         this.set('connexion', false);
      }
    }
  },
  willUpdate() {
    if (this.errorLogin && this.inscription && this.connexion) {
      alert('put1');
      var _this = this;
      _this.$('#register .bash').each(function() {
        $(this).find('p').clone().appendTo('.ticket2');
        $(this).children('input').clone().appendTo('.ticket2').replaceWith(function() {
          if ($(this).attr('name') == "password") {
            console.log('password');
            var password = $(this).val();
            var cleanPassword = "";
            var i;
            for (i = 0; i < password.length; i++) {
              cleanPassword += "*";
            }
            return $('<p>' + cleanPassword + '</p>');
          } else {
            return $('<p>' + $(this).val() + '</p>');
          }
        });
        $(this).children('input').val('');
      //   this.$('<p class="error">' + this.errorMessage + '</p>').appendTo('.ticket2');


      });
        this.toggleProperty('welcome');

      this.$('<p class="error">' + this.errorMessage + '</p><p class="error">VOUS AVEZ PERDU VOTRE MOT DE PASSE ?</p><p class="error">TAPER !helpMe POUR RECEVOIR CHANGER VOTRE MOT DE PASSE</p>').appendTo('.ticket2');
      // this.set('connexion', false);
      var newModel = this.get('model');
      this.set('model', newModel);

      this.toggleProperty('inscription2');
      this.toggleProperty('inscription3');
      this.toggleProperty('inscription4');


    }
    if(this.registered){
      this.set('inscription',false);
      this.set('step1',true);
      // this.set('registered',false);
   }



  },

  actions: {
    sendToRegister() {
      if (this.get('inscription1') && this.get('inscription4')) {
        console.log('sendToRegister');
        this.sendAction('register', this.model);
        this.set('connexion', true);
      };
    },
    sendToAuthenticate() {
      console.log("sendToAuthenticate");
      if (this.get('step1') && this.get('step2')) {
        var credentials = this.getProperties('identification', 'password');
        var identification = this.$("#login > p");
        identification.appendTo('.ticket');
        var self = this;
        this.$('#login .bash').each(function() {
          $(this).children('p').clone().appendTo('.ticket');
          $(this).children('input').clone().appendTo('.ticket').replaceWith(function() {
            if ($(this).attr('name') == "password") {
              console.log('password');
              var password = $(this).val();
              var cleanPassword = "";
              var i;
              for (i = 0; i < password.length; i++) {
                cleanPassword += "*";
              }
              return $('<p>' + cleanPassword + '</p>');
            } else {
              return $('<p>' + $(this).val() + '</p>');
            }
          });
        });
        this.set('step1', false);
        this.set('step2', false);
        this.set('connexion', true);
        this.$('#login .bash').find('input').val('');
        // .clone().appendTo(".ticket")
        this.sendAction('redirect', credentials);
      }
    },
    bashOne(val) {
      console.log('bashOne');
      console.log(val);
      // this.set('connexion', false);
      this.set('errorLogin', false);
      if (val == "newUser") {
        this.toggleProperty('inscription');
        this.toggleProperty('inscription1');
        this.toggleProperty('step1');
      }
      else if(val == ""){

      }
      else if(val =='newuser'){

         }

      else {
         var re = /\S+@\S+\.\S+/;
         if (re.test(val)) {
            this.toggleProperty('step2');
            console.log('property ' + this.step2);
            this.$('#password').focus();
         } else {
           var error = '<p>* ' + val + "</p><p class='error'>* OUPS! votre email n'est pas valide</p>";
           $(error).insertBefore('#identification');
           this.$('#identification').val('');
           // $(this).children('input').clone().appendTo('.ticket').replaceWith(function() {
           //    return $('<p>' + $(this).val() + '</p><p>'+ error+'</p>');
           // });
         }


      }
    },
    bashTwo(val) {
      console.log('bashTwo');
      if(val == 'exit'){
         this.toggleProperty('inscription');
         this.toggleProperty('inscription1');
         this.set('identification', '');
         $('#newIdentification').val('');
         this.toggleProperty('step1');
      }
      else{
         this.toggleProperty('inscription2');
      }
    },
    bashTree(val) {
      if(val == 'exit'){
         this.set('step1', false);
         this.set('inscription2', false);
         this.set('identification', '');

         console.log('exit bashTree');
         return;
      }
      var re = /\S+@\S+\.\S+/;
      if (re.test(val)) {
        this.toggleProperty('inscription3');
        console.log(val);
      } else {
        var error = '<p>' + val + "</p><p class='error'>* votre email n'est pas valide</p><p>* TAPER VOTRE EMAIL & APPUYER SUR ENTRER</p>";
        $(error).insertBefore('#newMail');
        this.$('#newMail').val('');
        // $(this).children('input').clone().appendTo('.ticket').replaceWith(function() {
        //    return $('<p>' + $(this).val() + '</p><p>'+ error+'</p>');
        // });
      }
    },
    bashFour(val) {
      console.log('bashFour');
      this.toggleProperty('inscription4');
      console.log(val);
    },
    bashFive(val) {
      console.log('bashFive');
      if(val == "exit"){
         this.set('step2', false);
         this.set('identification', '');
                  this.set('password', '');
      }
      else{
         this.$('#login').submit();

      }
    },
    bashSix(val) {
      console.log('bashSix');
      if (this.$('#newPassword').val() == this.$("#password").val()) {
        this.$('#register').submit();
        this.set('identification', "");
      } else {
         var error = '<p>' + val + "</p><p class='error'>* vos mot de passe ne sont pas identique</p><p>* CHOISIR VOTRE MOT DE PASSE & APPUYER SUR ENTRER</p>";
         $(error).insertBefore('#newPassword');
         this.set('password', "");
         this.set('password_confirm',"");
         $('#newPassword').val('');
         $('#password').val('');

        this.toggleProperty('inscription5');
        this.toggleProperty('inscription4');
      }
    }
  },

});
