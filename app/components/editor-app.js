import Ember from 'ember';

const {
  service
} = Ember.inject;

export default Ember.Component.extend({
  store: service('store'),
  ajax: service(),
  selectedImage: '',
  selectedImageData: Ember.computed('selectedImage', 'pin', function() {
    var selectId;
    console.log('PUTAINNNNNNNNNNNNNNN');
    if (this.get('selectedImage') != "") {
      selectId = this.get('selectedImage');
      console.log('selectId' + selectId);
      //  var image = this.get('imagesList').filterBy('id',selectId);
      var image = this.get('store').peekRecord('image', selectId);
      return image.get('imageData');
    } else {
      return '';
    }
  }),
  activeId: "home",
  loadedImage: 0,
  isLoading: true,
  unSave: false,
  isPreview: false,
  isFullscreen: false,
  isBlackMode: false,
  isPalette: false,
  isHeader: true,
  currentScreen: false,
  currentPixel:'-:-',
  errorMessage: null,
  compileErrors: [],

  maxLines: 15,

  functions: Ember.computed('project', function() {
    return this.get('project.functions');
  }).property(),

  colors: [{
    'code': '#00CCFF'
  }],


  init() {
    this._super(...arguments);
    console.log('INIT | isLoading', this.get('isLoading'));
   console.log('INIT | loadedImage', this.get('loadedImage'));

  },
  projectLength: Ember.computed('project', function() {
    return this.get('project.images.length');
  }),
  didInsertElement() {
    let _this = this;
    $(".holder").on('mousewheel', function(e) {
      var speed = 50;
      if (_this.isFullscreen) {
        speed = 0;
      }

      var keyframes = document.getElementsByClassName("keyframes")[0];

      var offset = $('.keyframes').offset().left;
      var road = $('.keyframes').width() - $('.holder').width();
      if (e.originalEvent.wheelDelta / 120 > 0) {

        // console.log('1', Math.abs(offset) + '|' + $('.keyframes').width()-$('.holder').width() );
        if (offset < 0) {
          if (speed != 0) {
            $('.keyframes').css('margin-left', offset + speed);
            var shuttleOffset = (offset - 50) * $('.holder').width() / $('.keyframes').width();
            $("#shuttle").css('margin-left', shuttleOffset * -1);
          }
        }
      } else {

        if (Math.abs(offset) < road) {
          if (speed != 0) {
            $('.keyframes').css('margin-left', offset - speed);
            var shuttleOffset = (offset - 50) * $('.holder').width() / $('.keyframes').width();
            $("#shuttle").css('margin-left', shuttleOffset * -1);
          }
        }
      }
    });

  },

  willDestroyElement() {
    this._super(...arguments);
   this.set('isLoading', true);
   this.set('loadedImage', 0);
   // this.get('store').unloadAll('image');
},

  imagesList: function() {
    return this.get("project.images");
  }.property(),

   pinboard : Ember.computed.filterBy('imagesList', 'pin', true),
   fullboard : Ember.computed.filterBy('imagesList', 'fullscreen', true),

  projectColors: Ember.computed('imagesList.@each.imageData', function() {
     var colors = [];
    this.get('imagesList').forEach(function(image){
      image.get('imageData.pixels').forEach(function(pixel){
         // image.imageData.forEach(function(pixel){
            colors.push(pixel.color);
         });
      var objects = image.get('imageData.objects');
      if(objects){
         objects.forEach(function(object){
            object.pixels.forEach(function(pixel){
                  colors.push(pixel.color);
               });
         // image.imageData.forEach(function(pixel){
         });
      }

         });
         let result = colors.reduce((p, c) => {
  if (!p.includes(c)) p.push(c);
  return p;
}, []);
      var response = [];
      result.forEach(function(color){
         response.pushObject({"code":color});
      });

         return response;
    }),

  projectData: Ember.computed.mapBy('imagesList', 'id'),

  actions: {
    setCurrentPixel(pixel){
        this.set('currentPixel', pixel);
     },
    shuttle() {
      var start = event.screenX;

      var self = this;
      var moove = self.startTravel;
      window.addEventListener('mousemove', moove, false);
      window.addEventListener('mouseup', function() {
        window.removeEventListener('mousemove', moove, false);

      });
    },
    sendLoadedImage(data) {
      this.incrementProperty('loadedImage');
      console.log('imageLoaded' + this.get('loadedImage'));
      if (this.get('loadedImage') == this.get('projectLength')) {
        this.resizeShuttle();
        this.set('isLoading', false);
        this.set('loadedImage', 0);
      }
    },

    addImage(projectId) {
      let project = this.get('store').peekRecord('project', projectId);
      let position = project.get('images').get('length');
      let data = {"image":position, "pixels": []};
      let image = this.get('store').createRecord('image', {
        "project": project,
        "position": position,
        "imageData": data,
        "imageFile": JSON.stringify(data,null,'\t')
      });
      image.save();
    },

    addPin: function(image) {
      image.toggleProperty('pin', true);
      if (image.pin) {
        this.set('activeId', "pannel-" + image.id);
      } else {
        this.set('activeId', "home");
      }
    },

    showConsole: function(image) {
      image.toggleProperty('console', true);
    },

    closeFullScreen: function() {
      this.exitFullscreen();
      this.toggleProperty('isFullscreen');
      this.set('currentScreen', false);
      $('.keyframes').removeClass('fullscreen');
      $('.keyframes').css('margin-left', 0);
    },

    addFullscreen: function(image) {
      if(this.isFullscreen){
         this.exitFullscreen();
         this.toggleProperty('isFullscreen');
         $('.keyframes').removeClass('fullscreen');
         $('.keyframes').css('margin-left', 0);
      }
      else{
         this.set('isFullscreen', true);
         var list = this.get('imagesList');
         var index = list.indexOf(image);
         this.set('currentScreen',index);
         if(index+1 < list.get('length')){
            var imageNext = list.objectAt(index +1);
            console.log('imageNext',imageNext.id);
            imageNext.set('fullscreen', true);
         }
         if(index-1 >= 0){
            var imagePrev = list.objectAt(index -1);
            console.log('imagePrev',imagePrev.id);
            imagePrev.set('fullscreen', true);
         }
         if(this.$('.keyframes').hasClass('little')){
            var header = document.getElementsByClassName('app-header')[0];
            header.style.display = "block";
            this.$('.keyframes.little').removeClass('little');
         }

         image.set('fullscreen', true);

         var editor = document.getElementById('editor-' + image.id);
         var keyindex = $(".image-editor").index(editor);
         if(index == 0){
            var travel = 0;
         }
         else{
            // var travel = $(editor).parent().offset().left - $(editor).offset().left + $(editor).width() - (((window.innerWidth*90/100)-(30*index-1)));
            var travel = -((450+30)*(index-1))-(window.innerWidth*90/100) ;
         }
        $('.keyframes').css('margin-left', travel).addClass('fullscreen');
      }
      //   $('.keyframes').addClass();
      // } else {
      // //   $('.keyframes').removeClass('fullscreen');
      //   $('.keyframes').css('margin-left', 0);
      // }
      var box1 = document.getElementsByClassName('visual-editor');
      var box2 = document.getElementsByClassName('data-editor');
      // var box3 = document.getElementsByClassName('color-palette');
      box1[0].style.height = "100%";
      box2[0].style.height = "0%";

      this.resizeShuttle();
      //change keyframes position
    },

    nextImage() {
      var list = this.get('imagesList');
      var index = this.get('currentScreen');

      if (this.get('currentScreen') - 1 >= 0) {
        var imageOld = list.objectAt(this.get('currentScreen') - 1);
        imageOld.set('fullscreen', false);
      }
          var number = Number(index)-2;
          var prevOffset = $('.image-editor:eq('+number+')').offset().left;
          var prevWidth = $('.image-editor:eq('+number+')').width();


      this.incrementProperty('currentScreen');
      index = this.get('currentScreen');
      var image = list.objectAt(index);
          image.set('fullscreen', true);

      if(index+1 < list.get('length')){
      var imageNext = list.objectAt(index+1);
         imageNext.set('fullscreen', true);
         }
      if(index-1 >= 0){
         var imagePrev = list.objectAt(index -1);
         imagePrev.set('fullscreen', true);
      }


      var travel = -((450+30)*(index-1))-(window.innerWidth*90/100) ;

      $('.keyframes').css('margin-left', travel );
    },

    prevImage() {
      var list = this.get('imagesList');
      var index = this.get('currentScreen');
      if (this.get('currentScreen') + 1 <= list.length) {
        var imageOld = list.objectAt(this.get('currentScreen') - 1);
        imageOld.set('fullscreen', false);
      }

      this.decrementProperty('currentScreen');
      index = this.get('currentScreen');

      if(index-1 >= 0){
         var imagePrev = list.objectAt(index -1);
         console.log('imagePrev', imagePrev.id);
         imagePrev.set('fullscreen', true);
      }
      if(index-1 < 0){
         var travel = 0;
      }
      else{
      var travel =-((450+30)*(index-1))-(window.innerWidth*90/100);
      }
      $('.keyframes').css('margin-left', travel);
    },


    askDeleteImage:function(image){
      image.toggleProperty('confirm', true);
   },
   keepImage:function(image){
     image.set('confirm', false);
 },
    deleteImage: function(image) {
      this.get('store').findRecord('image', image.id, {
        backgroundReload: false
      }).then(function(image) {
        image.destroyRecord();
      });
      this.resizeShuttle();
    },

    valueUpdated: function(id, imageFile) {
      console.log('-- valueUpdated');
      var stringified = JSON.stringify('imageFile');
      var image = this.get('store').peekRecord('image', id);
      if (imageFile == "") {
        console.log("pute");
        image.set('imageFile', '{"pixels":[]}');
        image.set('imageData', {
          "pixels": []
        });
        return;
      }
      try {
        console.log('try');
        var parse = JSON.parse(imageFile);
        console.log('parsed');
        image.set('imageData', JSON.parse(imageFile));
        image.set('isError', false);
        image.set('imageFile', imageFile);
        this.set('unSave', true);


      } catch (e) {
        console.log(e);

        var error = String(e).split(':');
        var errorMessage = error[1];
        if (errorMessage == " Unexpected end of JSON input") {
          errorMessage = "Fin inatendue";
        } else {
          var regex = /(Unexpected token)(...)(in)+/g;
          errorMessage = regex.exec(errorMessage);
          if (errorMessage) {
            errorMessage = 'Caractère "' + errorMessage[2] + '" inatendu ';
          }

        }


        image.set('errorMessage', errorMessage);
        image.set('isError', true);
        //   });
      }
    },

    save: function() {
      this.project.save();
      this.set('unSave', false);
    },

    initlizeResize: function() {

      var self = this;
      var resize = self.startResizing;

      this.$(".resizer").addClass('active');
      window.addEventListener('mousemove', resize, false);
      window.addEventListener('mouseup', function() {
        window.removeEventListener('mousemove', resize, false);
        this.$(".resizer").removeClass('active');

        var editorHeight = Ember.$('.data-editor').height();
        var nbrLine = (editorHeight - 50) / 17;
        self.set('maxLines', nbrLine);
        self.resizeShuttle();
      });
    },
    seePreview: function() {
      this.toggleProperty('isPreview', true);
    },

    closePreview: function() {
      this.toggleProperty('isPreview');
    },

    //  projectUpdated: function(value) {
    //    var projectData = JSON.parse(value);
    //    projectData.forEach(function(image) {
    //      console.log(image.image);
    //    });
    //  },
    functionUpdated: function(value) {
      this.project.set('functions', value);
    },

    toggleMenuHandler: function() {
      this.sendAction('toggleMenu');
    },
    saveExport: function(value){
      console.log('saveExpofg');
      this.sendAction('setExport',value);
      },
    togglePalette: function() {
      this.toggleProperty('isPalette');
    },
    toggleBlackMode: function() {
      this.toggleProperty('isBlackMode');
    },
    pretify: function(image) {
      image.set('imageFile', JSON.stringify(image.get('imageData'), null, "\t"));
    },
    findAndReplace: function(image) {
      var value = image.get('imageFile');
      var change = JSON.stringify(JSON.parse(value).pixels);

      var re = new RegExp(this.find, 'g');
      var counts = (change.match(re) || []).length;
      var newValue = change.replace(re, this.replace);
      value = JSON.parse(value);
      value.pixels = JSON.parse(newValue);
      image.set('imageData', value);
      image.set('imageFile',  JSON.stringify(value,null, '\t'));
    },
    findAllAndReplace: function(image) {
      var value = image.get('imageFile');
      var re = new RegExp(this.find, 'g');
      var counts = (value.match(re) || []).length;
      var newValue = value.replace(re, this.replace);
      image.set('imageData', JSON.parse(newValue));
      image.set('imageFile', newValue);
   },
   draftUpdate:function(value){
      var project = this.get('project');
      project.set('draft', value);
   },
   scriptUpdate:function(value){
      var list = this.get('projectData');
      var _this=this;
      var store = this.get("store");
      var projectId =  this.get('project.id');
      // console.log("value",value.length);
      //    console.log("value",value);
      // console.log('imageList', this.get("imagesList"));

      this.get('imagesList').forEach(function(image){
            var imageData = image.get('imageData');
               imageData.objects = [];
               image.set('imageFile', JSON.stringify(imageData,null, '\t'));
               image.set('imageData', JSON.parse(JSON.stringify(imageData,null, '\t')));
            });

      value.forEach(function(imageUpdate){
         // console.log('images.forEach', imageUpdate.image);
         var imagePosition = Number(imageUpdate.image) - 1;
         var imageId =  list[Object.keys(list)[imagePosition]];
         if(imageId == undefined){

            var error = {};
            error.imageId = imagePosition+1;
            error.object = "vous devez ajouter une image pour compléter l'animation";
            _this.compileErrors.addObject(error);
                        return false;
            // var addingImage = confirm('Ajouter une image ?');
            // if(addingImage == true){
            //    _this.send("addImage", projectId);
            }
         //
         // }else{


         var image = store.peekRecord('image', imageId);
         var imageData = image.get('imageData');
         // console.log('imageData', imageData);
            imageData.objects = imageUpdate.objects;
            image.set('imageFile', JSON.stringify(imageData,null, '\t'));
            image.set('imageData', JSON.parse(JSON.stringify(imageData,null, '\t')));

      });
            _this.$('.btn-event-function').attr('data-loading', 'false');
            // console.log("value",value.length);
            // console.log("value",value);
      // this.project.save();
      // this.set('unSave', false);
   },


  },

  resizeShuttle: function() {
    var holder = document.getElementsByClassName("holder")[0];
    var keyframes = document.getElementsByClassName("keyframes")[0];
    var shuttle = document.getElementById('shuttle');
    shuttle.style.width = holder.offsetWidth / keyframes.offsetWidth * holder.offsetWidth + 'px';
    if (holder.offsetWidth > keyframes.offsetWidth) {
      shuttle.style.padding = '0px';
    } else {
      shuttle.style.padding = '5px 0px';
    }
  },
  startTravel: function() {
    var holder = document.getElementsByClassName("holder")[0];
    var keyframes = document.getElementsByClassName("keyframes")[0];
    var shuttle = document.getElementById('shuttle');
    if (shuttle.offsetLeft > 0 || shuttle.offsetLeft <= holder.clientWidth - shuttle.clientWidth) {
      shuttle.style.marginLeft = event.clientX - shuttle.clientWidth / 2 + 'px';
      keyframes.style.marginLeft = -Math.abs(keyframes.clientWidth / holder.clientWidth * shuttle.offsetLeft) + 'px';
    }
  },
  // driveShuttle: function() {
  //
  // },
  startResizing: function(_this) {

    var box1 = document.getElementsByClassName('visual-editor');
    var box2 = document.getElementsByClassName('data-editor');
    var app = document.getElementsByClassName('wrap-app');
    var editor = document.getElementsByClassName('ace_editor');
    var header = document.getElementsByClassName('app-header')[0];
    var keyframes = document.getElementsByClassName('keyframes')[0];
    //  var aceEditor = document.getElementsByClassName('ace_editor');
    var editorContent = document.getElementsByClassName('ace_content');
    var editorInput = document.getElementsByClassName('ace_text-input');

    box1[0].style.height = (event.clientY - box1[0].offsetTop) + 'px';
    box2[0].style.height = (app[0].clientHeight - event.clientY) + 'px';

    if (box2[0].style.height < 30) {
      box2[0].style.height = "50px";
    }
    //  aceEditor.style.height = box2[0].clientHeight+'px';
    if ((event.clientY < screen.height / 2) && (header.clientHeight != 0)) {
      //header.style.display = "none";
      keyframes.className += " little";
      // Ember.resizeShuttle();
    } else if ((event.clientY > screen.height / 2)) {
      //header.style.display = "block";
      keyframes.classList.remove("little");
      // Ember.resizeShuttle();
    }
  },


  exitFullscreen: function(){
   var fullboard =this.get('fullboard');
   fullboard.forEach(function(image){
      image.set('fullscreen', false);
   });
 }


});
