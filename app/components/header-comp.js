import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';

export default Ember.Component.extend({

   dropdownOpen: false,

   actions: {
       toggleDropdown() {
           this.toggleProperty('dropdownOpen');
       },
       invalidateSession() {
          this.get('session').invalidate();
       }
   }

});
