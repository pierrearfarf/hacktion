import Ember from 'ember';

export default Ember.Component.extend({
   store: Ember.inject.service(),
   textData : Ember.computed('imagesList.@each', function(){
      console.log('pute');
      var code = this.get('imagesList').mapBy('imageData');
   return code;
   }),
   imagesList : Ember.computed('project', function(){
      return this.get('project.images');
   }),
   actions: {
      updateImageData(val) {
         var input = document.getElementById('textImage');
         console.log(input);
         var imageId = Number(input.dataset.parent);
         console.log(imageId);
         var that = this;
         var image = this.get('store').peekRecord('image', imageId);
         console.log(image);

           try {
             image.set('imageData', JSON.parse(input.value));
            that.set('imagesList',that.get('project.images'));
            console.log("imageList" + that.get('imagesList'));
           } catch (e) {
               console.log(e);
           }
         }

   }
});
