import Ember from 'ember';


export default Ember.Component.extend({

  didInsertElement() {
    this._super(...arguments);
      this.set('continueAnimating', true);
    this.drawSprite(this);

  },
didReceiveAttrs(){    this._super(...arguments);},
  textData: Ember.computed('data.@each', function() {
    var code = this.get('data').mapBy('imageData');
    console.log("code",code);
    return code;
  }),
  currentFrame: 0,
  continueAnimating: false,
  totalFrames: null,
  isExport: false,
  fps: 500,
  finalGif:null,


  drawSprite: function(context) {
    var c = document.getElementById("myCanvas");
    var ctx = c.getContext("2d");
    var requestAnimationFrame = window.requestAnimationFrame ||
      window.mozRequestAnimationFrame ||
      window.webkitRequestAnimationFrame ||
      window.msRequestAnimationFrame;

    // var data = JSON.parse(this.get('textData'));
    var data = context.get('textData');
    var nbrCol = context.get('project.width');
    var width = 720;
    var pixelSize = width/nbrCol;
    var height = 450;
    var looping = document.getElementById("animationLoop").checked;
    var totalFrames = context.set('totalFrames', data.length);
    var fps = this.fps;
    var grid = this.get('project.grid');
    var gridColor = this.get('project.gridcolor');

    if (context.get('continueAnimating') == false) {
      return;
    }
    setTimeout(function() {
      var view = this;
      requestAnimationFrame(function() {
        context.drawSprite(context)
      });
      //reinitialize img
      ctx.clearRect(0, 0, width, height);
      ctx.fillStyle = context.get('project.bgcolor');
      ctx.fillRect(0, 0, width, height);


      var pixels = [];
      var objects =  data[context.get('currentFrame')].objects;
      if(objects){
        objects.forEach(function(object){
         //   pixels = object.pixels.concat(pixels);
             pixels = pixels.concat(object.pixels);
         //   pixels.reverse();
        });
     }
     pixels = pixels.concat( data[context.get('currentFrame')].pixels);

      pixels.forEach(function(pixel) {
         var coord = pixel.id.split(':');
         var x = coord[0]-1;
         var y = coord[1]-1;

        ctx.fillStyle = pixel.color;
        ctx.fillRect(x * pixelSize, y * pixelSize, pixelSize, pixelSize);
        ctx.stroke();
        ctx.fillStyle = pixel.color;
        ctx.fillRect(x * pixelSize, y * pixelSize, pixelSize, pixelSize);
        ctx.stroke();
        ctx.fillStyle = pixel.color;
        ctx.fillRect(x * pixelSize, y * pixelSize, pixelSize, pixelSize);
        ctx.stroke();
        ctx.fillStyle = pixel.color;
        ctx.fillRect(x * pixelSize, y * pixelSize, pixelSize, pixelSize);
        ctx.stroke();
        ctx.fillStyle = pixel.color;
        ctx.fillRect(x * pixelSize, y * pixelSize, pixelSize, pixelSize);
        ctx.stroke();
      });
      if(grid){
         for (var i = 1; i < width; i++) {
            ctx.beginPath();
               ctx.moveTo(i*pixelSize, 0);
               ctx.lineTo(i*pixelSize, height*pixelSize);
               context.lineWidth = 1;
               ctx.strokeStyle = gridColor;
               ctx.stroke();
               ctx.moveTo(i*pixelSize, 0);
               ctx.lineTo(i*pixelSize, height*pixelSize);
               context.lineWidth = 1;
               ctx.strokeStyle = gridColor;
               ctx.stroke();
         }
         for (var i = 1; i < height; i++) {
               ctx.beginPath();
               ctx.moveTo( 0,i*pixelSize);
               ctx.lineTo( width*pixelSize,i*pixelSize);
               context.lineWidth = 1;
               ctx.strokeStyle = gridColor;
               ctx.stroke();
               ctx.beginPath();
               ctx.moveTo( 0,i*pixelSize);
               ctx.lineTo( width*pixelSize,i*pixelSize);
               context.lineWidth = 1;
               ctx.strokeStyle = gridColor;
               ctx.stroke();
         }
      }

      if (context.get('currentFrame') == Number(totalFrames) - 1) {

        if (looping == true) {
          // var currentFrame = this.get('currentFrame');
          context.set('currentFrame', 0);
        } else {
          context.set("continueAnimating", false);
          //  continueAnimating = false;
        }

      } else {
        // currentFrame++;
        context.incrementProperty('currentFrame');

      }
    }, fps);
  },

  actions: {
    stopAnimate: function() {
      this.set('continueAnimating', false);
    },
    startAnimate: function() {
      this.set('continueAnimating', true);
      this.drawSprite(this);
    },
    updateFps: function(value) {
      this.set('fps', value);
    },
    export: function() {
      this.set('currentFrame', 0);
      var _this = this;
      var canvas = document.getElementById("myCanvasExport");
         _this.toggleProperty('isExport');
      this.exportation(canvas, this.get("textData"), this.fps, this.get('project.width'),this.get('project.bgcolor'), this.get('project.grid'), this.get('project.gridcolor')).then(function(value) {
         _this.set('finalGif', value);

         _this.sendAction('saveExport',value);
      });
    },
  },

  exportation: function(canvas,data,fps,nbrCol ,bgcolor, grid, gridcolor) {
   return new Promise(function(resolve, reject) {
    var encoder = new GIFEncoder();
    var totalFrames = data.length-1;
    var keepGoing = true;
    var width = 1280;
    var height = 800;
    var pixelSize = width/nbrCol;

    var c = canvas;
    var ctx = c.getContext("2d");


    encoder.setRepeat(0); //auto-loop
    encoder.setDelay(fps);
    encoder.start();
    var _this = this;
    var currentFrame = 0;
      var image = false;
    var encoding = function() {

      if (keepGoing) {
        setTimeout(function() {
           requestAnimationFrame(encoding);
         ctx.clearRect(0, 0, width, height);
         ctx.fillStyle = bgcolor;
         ctx.fillRect(0, 0, width, height);


          var pixels = [];
          var objects =  data[currentFrame].objects;
          if(objects){
            objects.forEach(function(object){
               pixels = pixels.concat(object.pixels);
            });
         }
         pixels = pixels.concat(data[currentFrame].pixels);


          pixels.forEach(function(pixel) {
             var coord = pixel.id.split(':');
             var x = coord[0];
             var y = coord[1];

            ctx.fillStyle = pixel.color;
            ctx.fillRect((x * pixelSize)-pixelSize, (y * pixelSize)-pixelSize, pixelSize, pixelSize);
            ctx.stroke();
            ctx.fillStyle = pixel.color;
            ctx.fillRect((x * pixelSize)-pixelSize, (y * pixelSize)-pixelSize, pixelSize, pixelSize);
            ctx.stroke();
            ctx.fillStyle = pixel.color;
            ctx.fillRect((x * pixelSize)-pixelSize, (y * pixelSize)-pixelSize, pixelSize, pixelSize);
            ctx.stroke();
            ctx.fillStyle = pixel.color;
            ctx.fillRect((x * pixelSize)-pixelSize, (y * pixelSize)-pixelSize, pixelSize, pixelSize);
            ctx.stroke();
            ctx.fillStyle = pixel.color;
            ctx.fillRect((x * pixelSize)-pixelSize, (y * pixelSize)-pixelSize, pixelSize, pixelSize);
            ctx.stroke();
          });
          if(grid){

             for (var i = 1; i < width; i++) {
                ctx.beginPath();
                   ctx.moveTo(i*pixelSize, 0);
                   ctx.lineTo(i*pixelSize, height);
                   ctx.lineWidth = 1;
                   ctx.strokeStyle = gridcolor;
                   ctx.stroke();
                       ctx.beginPath();
                   ctx.moveTo(i*pixelSize, 0);
                   ctx.lineTo(i*pixelSize, height);
                   ctx.lineWidth = 1;
                   ctx.strokeStyle = gridcolor;
                   ctx.stroke();
                   ctx.beginPath();
              ctx.moveTo(i*pixelSize, 0);
              ctx.lineTo(i*pixelSize, height);
              ctx.lineWidth = 1;
              ctx.strokeStyle = gridcolor;
              ctx.stroke();
              ctx.beginPath();
          ctx.moveTo(i*pixelSize, 0);
          ctx.lineTo(i*pixelSize, height);
          ctx.lineWidth = 1;
          ctx.strokeStyle = gridcolor;
          ctx.stroke();
             }
             for (var i = 1; i < height; i++) {
                   ctx.beginPath();
                   ctx.moveTo( 0,i*pixelSize);
                   ctx.lineTo( width,i*pixelSize);
                   ctx.lineWidth = 1;
                   ctx.strokeStyle = gridcolor;
                   ctx.stroke();
                   ctx.beginPath();
                   ctx.moveTo( 0,i*pixelSize);
                   ctx.lineTo( width,i*pixelSize);
                   ctx.lineWidth = 1;
                   ctx.strokeStyle = gridcolor;
                   ctx.stroke();
                   ctx.beginPath();
                   ctx.moveTo( 0,i*pixelSize);
                   ctx.lineTo( width,i*pixelSize);
                   ctx.lineWidth = 1;
                   ctx.strokeStyle = gridcolor;
                   ctx.stroke();
                   ctx.beginPath();
                   ctx.moveTo( 0,i*pixelSize);
                   ctx.lineTo( width,i*pixelSize);
                   ctx.lineWidth = 1;
                   ctx.strokeStyle = gridcolor;
                   ctx.stroke();
             }
          }
          if (currentFrame === totalFrames) {
            keepGoing = false;
            encoder.addFrame(ctx);
            encoder.finish();

            image = 'data:image/gif;base64,' + encode64(encoder.stream().getData());
            // return image;
            console.log("finish !");
            // this.$('#gifExport').attr('src', 'data:image/gif;base64,' + encode64(encoder.stream().getData())).css('border-color', 'blue');



          } else {
            encoder.addFrame(ctx);
          }
          //  context.incrementProperty('currentFrame');
          currentFrame++;

        }, fps);
      } else {

      resolve(image);

      }

   }
      encoding();
   });
   }

});
