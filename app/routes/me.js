import Ember from 'ember';
import ApplicationRouteMixin from 'ember-simple-auth/mixins/application-route-mixin';


const { service } = Ember.inject;
export default Ember.Route.extend(ApplicationRouteMixin, {
   session: service('session'),
   sessionAccount: service('session-account'),
   model(){
            return Ember.RSVP.hash({

               events : this.store.query('project', { filter: { 'user_id': this.get('sessionAccount.account.id') } }).then(function(user_projects) {
                        console.log(user_projects);
                        return user_projects;

                     })
        });

   },
   setupController(controller, models) {
         controller.setProperties(models);
    },

   // actions: {
   //    sendData(data) {
   //      alert("Data sent to route" + data);
   //    }

});
