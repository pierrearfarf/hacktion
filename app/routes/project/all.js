import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
const { service } = Ember.inject;

export default Ember.Route.extend(AuthenticatedRouteMixin, {

  model() {
      return Ember.RSVP.hash({
         projects:
            this.get('store').findAll('project'),
         });
  },

  setupController(controller, models) {
        controller.setProperties(models);
   },
   action:{

   }

});
