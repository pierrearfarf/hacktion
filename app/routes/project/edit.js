import Ember from 'ember';


import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

const { service } = Ember.inject;
export default Ember.Route.extend(AuthenticatedRouteMixin,{
   model(params){
      return Ember.RSVP.hash({
         project: this.store.findRecord('project', params.project_id)
         });
   },
   setupController(controller, model){
          controller.setProperties(model);
   },
   // imagesList:[],
   init(){
      this._super(...arguments);
   },

actions:{

}
});
