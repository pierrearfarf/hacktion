import Ember from 'ember';

import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {

  model() {
      return Ember.RSVP.hash({
         newproject: {}
         });
  },
  setupController(controller, models) {
        controller.setProperties(models);
   },
  actions: {
        save() {
          const newProject = this.get('store').createRecord('project', this.currentModel.newproject);
          newProject.save().then((project) => {
               this.transitionTo('project.edit', project.id);
            });
        },
        cancel() {
          this.transitionTo('index');
         }
      }
   });
