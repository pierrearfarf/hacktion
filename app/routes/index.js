import Ember from 'ember';
const { service } = Ember.inject;
export default Ember.Route.extend({
   session: service('session'),
   sessionAccount: service('session-account'),
   model(){
      return Ember.RSVP.hash({
         });
   },
   init(){
      this._super(...arguments);
   },
   setupController(controller, model){
          controller.setProperties(model);
   },
});
