import Ember from 'ember';
const { service } = Ember.inject;
export default Ember.Route.extend({
   session: service('session'),
sessionAccount: service('session-account'),
model(params){
   return Ember.RSVP.hash({
      project: this.store.findRecord('project', params.project_id, { reload: true }),
      });
},
setupController(controller, models){
       controller.setProperties(models);
}
});
