import Ember from 'ember';

import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';

export default Ember.Route.extend(AuthenticatedRouteMixin, {
   refreshMap: false,
   isLocated: false,
  model() {
      return Ember.RSVP.hash({
         sports: this.store.findAll('sport'),
         newevent: {}
         });
  },

  setupController(controller, models) {
        controller.setProperties(models);
   },

  actions: {
        save() {
          const newEvent = this.get('store').createRecord('event', this.currentModel.newevent);
          newEvent.save().then((event) => {
               this.transitionTo('event', event.id);
            });
        },
        cancel() {
          this.transitionTo('index');
         }
      }
});
