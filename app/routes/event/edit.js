import Ember from 'ember';
import AuthenticatedRouteMixin from 'ember-simple-auth/mixins/authenticated-route-mixin';
const { service } = Ember.inject;

export default Ember.Route.extend(AuthenticatedRouteMixin,{
   session: service('session'),
   sessionAccount: service('session-account'),
   model(params){

      return Ember.RSVP.hash({
         event: this.store.findRecord('event', params.event_id),
         });

   },
   afterModel: function(model, transition) {
      var createdBy =  model.event.data.createdBy.id;
      var accountId =  this.get('sessionAccount.account.id');


if(accountId != createdBy){
             this.transitionTo('index');
}


   },
   setupController(controller, models){
          controller.setProperties(models);
   },
   actions: {
         save() {
            // console.log();
            let self = this;
            this.store.findRecord('event', this.currentModel.event.id).then(function(event) {
              // ...after the record has loaded
            //   event.set('title', "Yollo");
              event.save().then(function() {
                self.transitionTo('event', event.id);
              });
            });
         },
         cancel() {
          this.transitionTo('index');
          }
      }
});
