import Ember from 'ember';

export default Ember.Controller.extend({

   session: Ember.inject.service('session'),
   sessionAccount: Ember.inject.service('session-account'),
   beforeModel() {
      this._super(...arguments);
     return this._loadCurrentUser();
   },
   sessionAuthenticated() {
     this._loadCurrentUser().then(()=>{
     }).catch(() => this.get('session').invalidate());
   },
   _loadCurrentUser() {
     return this.get('sessionAccount').loadCurrentUser();
   },

   actions: {
    invalidateSession() {
       this.get('session').invalidate();
    },


},



});
