import Ember from 'ember';
const { service } = Ember.inject;

export default Ember.Controller.extend({
   session: service('session'),
   sessionAccount: service('session-account'),


   init(){
      this._super(...arguments);
      console.log(this.get('sessionAccount.account.name'));
      if(!this.get('sessionAccount.account.name')){
         console.log('no account');
      var self = this;
      this.set('onPress', this.get('enterconsole').bind(this));
      window.addEventListener('keydown', this.get('onPress'), false);
            }
   },

   enterconsole:function(e){
      var pathArray = window.location.pathname.split( '/' );

      if(pathArray[1]== ""){
             e = e || window.event;
             if( e.keyCode == 67){
                     this.transitionToRoute('login');
                     window.removeEventListener('keydown', this.get('onPress'), false);
             }
         }

   }

});
