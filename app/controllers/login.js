import Ember from 'ember';
const { service } = Ember.inject;

export default Ember.Controller.extend({
   session: service('session'),
   sessionAccount: service('session-account'),
init(){
   this._super(...arguments);
},
errorMessage:null,
errorLogin:null,
registered:false,
connexion:false,
actions:{
   redirect: function(data) {


   var     authenticator = 'authenticator:jwt';
   var _this=this;

   this.set('errorMessage', null);
   this.get('session')
     .authenticate(authenticator, data)
     .then(function(){
       _this.transitionToRoute('/');
     }, function(message){
        console.log("data : " + message.result);
        _this.set('errorLogin', true);
        _this.set('errorMessage', message.result);
      //   _this.sendAction('setError','');

        //on reinitalise les computed proprety
        //on transfert le form dans ticket
        //on insert le nouveaux formulaire
   });
   //   .catch((message) => {
   //      console.log(message);
   //      this.set();
   //      this.set('errorMessage', message.result);
   //  })
   //  .then(()=>{console.log('authenticator'); });




   },
   register: function(data){
       let _this = this;
      //  this.get('model').save().then(
      //     function(){
      //        _this.set('registered',true);
      //       _this.transitionToRoute('/');
      //     },
      //     function(error){
      //        _this.set('errorMessage',error);
      //     });

       this.get('model').save().then(()=>{
           _this.set('registered',true);
           this.set('connexion', false);

         //  _this.transitionToRoute('/');
       }).catch((error)=>{
                  _this.set('errorLogin', true);
                  console.log(error);
           _this.set('errorMessage',error.errors[0].detail);
       });
   }
}

});
