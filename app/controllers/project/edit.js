import Ember from 'ember';
import ENV from '../../config/environment';
const { service } = Ember.inject;
export default Ember.Controller.extend({
   session: service('session'),
   sessionAccount: service('session-account'),
   ajax: service(),
   init(){
      this._super(...arguments);
   },
   isNavVisible:false,
   actions:{
      toggleMenu() {
        this.toggleProperty('isNavVisible');
     },
     setExport(value){
        let projectId = Ember.get(this, 'project.id');
        let ajax = this.get('ajax');
        let userToken = 'Bearer '+this.get('session.data.authenticated.token');

         function dataURLtoBlob(dataurl) {
             var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
                 bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
             while(n--){
                 u8arr[n] = bstr.charCodeAt(n);
             }
             return new Blob([u8arr], {type:mime});
         }
         var blob = dataURLtoBlob(value);
         var fd = new FormData();
         fd.append("export", blob, "export.gif");

         return ajax.request(ENV.apiBaseUrl+'/api/project/'+projectId+'/picture', {
                       type: "POST",
                        data: fd,
                        processData: false,
                        contentType: false,
                        beforeSend: function(xhr){
                   xhr.setRequestHeader('Authorization', userToken)
               }});



     }
   }
});
