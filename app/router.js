import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {

     this.route('protected');
     this.route('login');
     this.route('inscription');
  this.route('index', { path: '/' });

     this.route('me');
     this.route('me.profil', { path: 'me/profil' }, function() {
       this.route('profil');
     });
     this.route('me.profil.edit', { path: 'me/profil/edit' }, function() {
       this.route('edit');
     });



     this.route('project.new', { path: '/project/new' }, function() {
       this.route('new');
     });

     this.route('project.edit', { path: '/project/:project_id/edit' }, function() {
      this.route('project.edit');
     });
     this.route('project.all', { path: '/project/all' }, function() {
      this.route('all');
     });
});

export default Router;
