import DS from 'ember-data';
import moment from 'moment';

export default DS.Transform.extend({
   deserialize: function (serialized) {
     if (serialized) {
       return moment(serialized).format("YYYY-MM-DD HH:mm");
     }
     return serialized;
   },

   serialize: function (deserialized) {
     if (deserialized) {
       return moment(deserialized).format("YYYY-MM-DD HH:mm:ss");
     }
     return deserialized;
   }
});
